FROM debian:sid
RUN apt update \
        && apt -yy install make libxft-dev libx11-dev gcc \
        && mkdir /builder \
        && cd /builder
CMD make

